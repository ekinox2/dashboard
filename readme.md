# Dash Application for Student Prioritization

## Overview

This repository contains a Dash application designed to prioritize students for personalized support based on their academic performance and other relevant factors. The application visualizes student data and provides insights to help educational advisors identify students who may benefit from additional assistance.

## Features

- Visualizes student data using interactive graphs and charts.
- Prioritizes students based on their current academic performance and complexity of support needed.
- Allows educational advisors to explore student data and identify areas for intervention.

## Installation

To run the Dash application locally, follow these steps:

1. Clone this repository to your local machine:

    ```bash
    git clone https://github.com/your-username/your-repository.git
    ```

2. Navigate to the project directory:

    ```bash
    cd your-repository
    ```

3. Install the required dependencies using pip:

    ```bash
    pip install -r requirements.txt
    ```

4. Run the Dash application:

    ```bash
    python app/app.py
    ```

5. Access the Dash application in your web browser at http://127.0.0.1:8050/.

## Testing

To run the tests for the Dash application, use the following command:

    ```bash
    pytest tests
    ```

This command will execute the tests defined in the tests directory and display the test results.

# Deployment


The Dockerfile included in this repository allows you to build and deploy the Dash application using Docker. Follow these steps to deploy the application with Docker:

1. Build the Docker image:

```bash
docker build -t my-dash-app .
```

2. Run the Docker container:

```bash
docker run -p 8050:8050 my-dash-app
```

This will start the Dash application in a Docker container, and you can access it in your web browser at http://localhost:8050/.

# Contributing

Contributions to this project are welcome! If you have suggestions for improvements or would like to report a bug, please open an issue on Gitlab.

## License

This project is licensed under the MIT License. See the LICENSE file for details.

```javascript
Replace placeholders such as `your-username`, `your-repository`, and `my-dash-app` with your actual GitHub username, repository name, and Docker image name, respectively.

This README provides an overview of the project, installation instructions, testing guidelines, deployment steps, contribution guidelines, and licensing information. You can expand or modify it further based on your project's needs.
```

