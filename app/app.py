import dash
from dash import dcc, html
import pandas as pd

# Import Data
df = pd.read_csv("../data/exercice_data.csv") 

# Calcul of indicators 
df['Complexity'] = (df['absences'] + df['Dalc'] + df['Walc'] + (5 - df['studytime']) + (5 - df['health'])) / 5

# Dashboard layout 
app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("Priorisation des élèves pour l'accompagnement"),
    dcc.Graph(
        id='student-prioritization-graph',
        figure={
            'data': [
                {
                    'x': df['FinalGrade'],
                    'y': df['Complexity'],
                    'mode': 'markers',
                    'marker': {
                        'color': 'rgba(50, 171, 96, 0.5)',  
                        'size': 10,
                        'line': {'width': 0.5, 'color': 'white'}
                    },
                    'type': 'scatter',
                    'text': df['StudentID'],  # use student id 
                    'hoverinfo': 'text'
                },
            ],
            'layout': {
                'title': 'Priorisation des élèves',
                'xaxis': {'title': 'Note finale en mathématiques'},
                'yaxis': {'title': 'Complexité d\'accompagnement'},
            }
        }
    )
])

# Execute Dash app 
if __name__ == '__main__':
    app.run_server(debug=True)
