# Use an official Python runtime as the base image
FROM python:3.11-slim

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

#Change directory to 
WORKDIR "/app"
RUN make

# Create data repo in the container 
RUN mkdir -p /data

# Copy data to container 
COPY ./data/exercice_data.csv ./data

# Install any needed dependencies specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Expose port 8050 to the outside world
EXPOSE 8050

# Run app.py when the container launches
CMD ["python", "app/app.py"]
