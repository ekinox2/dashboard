# tests/test_app.py

import sys 
import os 

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import pytest
from dash import Dash
from dash.testing.application_runners import import_app
from dash.testing.browser import Browser

# Import the Dash application from the app module
app = import_app('app.app')

@pytest.fixture
def dash_app():
    """Create a Dash test app."""
    yield app

@pytest.fixture
def dash_duo(dash_app):
    """Create a Dash test app with browser."""
    # Create a Dash test app with browser
    with Browser(app) as browser:
        yield browser



